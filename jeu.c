#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "jeu.h"
#include "regle.h"
#include "aff.h"
/* Auteur : Dorian */
/* Date :  23/05/2024 */
/* Résumé : Fonction qui retourne un tableau de la catégorie de chaque joueur (humain ou ordinateur) */
/* Entrée(s) : Nombre de joueurs présents dans le jeu  */
/* Sortie(s) : Retourne pour chaque joueur si ils sont humains ou pas  */

char* HumainOuOrdi(int nbJoueurs)
{
    char reponse;
    char* tab_rep = malloc(nbJoueurs*sizeof(char));
    for (int i=0; i<nbJoueurs; i++)
    {
        do
        {
            printf("Le joueur %d est-il humain ? (y/n)\n", i+1);
            scanf(" %c", &reponse);
            clear();
        } while ((reponse != 'y') && (reponse != 'n'));
        tab_rep[i] = reponse;
    }
    return tab_rep;
}

/* Auteur : Dorian */
/* Date :  23/05/2024 */
/* Résumé : Fonction qui initialise un pion */
/* Entrée(s) : Le numéro de la couleur choisie  */
/* Sortie(s) : Retourne le pion initialisé  */
Pion initPion(int couleur)
{
    Pion pion;
    pion.num_case = -couleur;
    pion.nb_case_franchies = 0;
    pion.couleur = couleur;
    return pion;
}

/* Auteur : Dorian */
/* Date :  23/05/2024 */
/* Résumé : Fonction qui initialise un joueur */
/* Entrée(s) : Joueur humain ou pas  */
/* Sortie(s) : Retourne le joueur initialisé  */
Joueur initJoueur(char reponse, int couleur)
{
    Joueur joueur;
    joueur.tab_pions = malloc(4*sizeof(Pion));
    joueur.compteur_six = 0;
    joueur.compteur_six_daffilee = 0;
    joueur.nb_pions_sortis = 0;
    joueur.nb_pions_arrives = 0;
    joueur.aBarrage = 0;
    joueur.couleur = couleur;
    joueur.num_dernier_pion_joue = 0;
    if (reponse == 'y')
    {
        joueur.humain = 1;
    }else
    {
        joueur.humain = 0;
    }
    for (int i=0; i<4; i++)
    {
        joueur.tab_pions[i] = initPion(joueur.couleur);
    }
    return joueur;
}


Joueur* InitTabJoueurs(char* tabreponse){
    Joueur* tab_joueurs=malloc(4*sizeof(Joueur));
    Pion** tab_allpions=malloc(4*sizeof(Pion*));        //tab_allpions est un tableau comprenant tout les pions de tous les joueurs
    for(int i=0; i<4; i++){
        tab_allpions=malloc(4*sizeof(Pion));
    }
    for(int i=0; i<4; i++)
    {
        tab_joueurs[i]= initJoueur(tabreponse[i], i);
        tab_allpions[i]= tab_joueurs[i].tab_pions;
    }
    return tab_joueurs;
}

/* Auteur : Oscar luiggi */
/* Date :  23/05 */
/* Résumé : demande a l'utilisateur quel pion il veut jouer */
/* Sortie(s) : retourne le num du pion  */
int choixPion(void){
    int res;
    do{
        printf("Quel pion voulez vous jouer ? (1, 2, 3 ou 4)\n");
        scanf("%d", &res);
        clear();
        res--;
    }while((res != 0) && (res != 1) && (res != 2) && (res != 3));
    return res;
}

/* Auteur : Oscar luiggi */
/* Date :  23/05 */
/* Résumé : Vider buffer*/

void clear(void){
    while(getchar() != '\n');
}
/* Auteur : moatez */
/* Sortie(s) : un nombre aleratoire */

int lancerD(void){
    int num;
    num = rand()%6+1;
    return num;
}

/* Auteur : Dorian */
/* Résumé : Procédure qui permet de gérer les règles lorsqu'on tire le numéro 6 */
/* Entrée(s) : Un joueur, la valeur du dé qu'il a tiré et le tableau de tous les pions  */
void GererReglesSix(Joueur* tab_j, Joueur j, Pion** tab_allpions, int num_j, int* a_gagne, int* gagnant)
{
    int caze;
    int num_pion;
    int nb_pions_sur_case;
    int m=-1;
    Pion* pions_case_depart;
    if ((j.compteur_six_daffilee == 3 ) && (!CazeRefuge(j.dernier_pion_joue.num_case))) // règle des 3 six d'affilée
    {
        tab_j[num_j].tab_pions[tab_j[num_j].num_dernier_pion_joue].num_case = -j.couleur;
        tab_j[num_j].tab_pions[tab_j[num_j].num_dernier_pion_joue].nb_case_franchies = 0;
        tab_j[num_j].nb_pions_sortis--;
        tab_j[num_j].compteur_six_daffilee = 0;
        printf("Vous avez fait 3 six d'affilée, votre pion retourne à la maison\n");
    }else
    {
        if (j.aBarrage) // casse le barrage (s'il y a) quand on fait 6
        {
            int* pion_formant_barrage = malloc(2*sizeof(int));            // tableau des pions constituant le barrage
            pion_formant_barrage = PionEnBarrage(j);
            if (PeutJouer(tab_allpions, j.tab_pions[pion_formant_barrage[0]], 6))
            {
                printf("Vous avez dû casser votre barrage !\n");
                tab_j[num_j].tab_pions[pion_formant_barrage[0]].num_case = j.tab_pions[pion_formant_barrage[0]].num_case+6;
                tab_j[num_j].tab_pions[pion_formant_barrage[0]].nb_case_franchies = j.tab_pions[pion_formant_barrage[0]].nb_case_franchies+6;
                tab_j[num_j].dernier_pion_joue = j.tab_pions[pion_formant_barrage[0]];
                tab_j[num_j].aBarrage=0; // le joueur n'a plus de barrage 
            }
        }else
        {
            int* liste_pion_maison=malloc(4*sizeof(int)); //liste des pions dans la maison
            int nb_pions_maison=0;
            int pion=-1;
            int pion_maison=0; // variable utile pour la condition d'après
            while (pion_maison<4 && j.tab_pions[pion_maison].num_case != -j.couleur)
            {
                pion_maison++;
            }
            while (pion<3)
            {
                pion++;
                if (j.tab_pions[pion].num_case == -j.couleur)
                {
                    liste_pion_maison[nb_pions_maison] = pion;
                    nb_pions_maison++;
                }
            }
            if (j.tab_pions[pion_maison].num_case == -j.couleur && MaisonOccup(tab_allpions, j) && j.humain) // règle de manger le pion se trouvant sur la case départ de la maison
            {
                num_pion = choixPion();
                do
                {
                    m++;
                    if (liste_pion_maison[m] == num_pion)
                    {
                        m=10;
                        caze = CazeDepart(j);
                        nb_pions_sur_case = CompterPionsCase(caze, tab_allpions);
                        pions_case_depart = PionsSurCaze(caze, tab_allpions);
                        if (!PionsMemeCouleur(j.tab_pions[num_pion], nb_pions_sur_case, pions_case_depart) && j.tab_pions[num_pion].num_case == -j.couleur) //on sait qu'il y a un pion de couleur differentes
                        {
                            for (int i=0; i<nb_pions_sur_case; i++)         // triple boucle pour vérifier quels pions manger
                            {
                                for (int k=0; k<4; k++)
                                {
                                    for (int n=0; n<4; n++)
                                    {
                                        if ((tab_j[k].couleur == pions_case_depart[i].couleur) && (tab_j[k].tab_pions[n].num_case == caze))
                                        {
                                            tab_j[k].nb_pions_sortis--;
                                            tab_j[k].tab_pions[n].num_case = -tab_j[k].couleur;     // change les informations induites par la regle
                                            tab_j[k].tab_pions[n].nb_case_franchies = 0;
                                        }
                                    }
                                }
                            }
                            tab_j[num_j].tab_pions[num_pion].num_case = CazeDepart(j);
                            tab_j[num_j].nb_pions_sortis++;
                            if (PeutJouer(tab_allpions, tab_j[num_j].tab_pions[num_pion], 20))
                            {
                                printf("Vous avez un bonus de 20 cases pour avoir mangé un pion adverse !\n");
                                tab_j[num_j].tab_pions[num_pion].num_case = (CazeDepart(j) + 20)%68;
                                tab_j[num_j].tab_pions[num_pion].nb_case_franchies = 21;
                            }else
                            {
                                printf("Un barrage bloque votre avancée, votre bonus de 20 cases est annulé\n");
                            }
                        }
                    }
                } while (liste_pion_maison[m] != num_pion && m<nb_pions_maison-1);
                if (m!=10)
                {
                    if (j.nb_pions_sortis < 4)
                    {
                        Jouer(tab_j, j, 6, tab_allpions, num_j, a_gagne, gagnant);
                    }
                    else  // si 4 pions sortis
                    {
                        Jouer(tab_j, j, 7, tab_allpions, num_j, a_gagne, gagnant);
                    }
                }
            }else // aucune règle particulière, on appelle la fonction jouer
            {
                if (j.nb_pions_sortis < 4)
                {
                    switch (j.humain)
                    {
                        case 0:
                        JouerRobot(tab_j, j, 6, tab_allpions, num_j, a_gagne, gagnant);
                        break;
                        case 1:
                        Jouer(tab_j, j, 6, tab_allpions, num_j, a_gagne, gagnant);
                        break;
                    }
                }
                else  // si 4 pions sortis
                {
                    switch (j.humain)
                    {
                        case 0:
                        JouerRobot(tab_j, j, 7, tab_allpions, num_j, a_gagne, gagnant);
                        break;
                        case 1:
                        Jouer(tab_j, j, 7, tab_allpions, num_j, a_gagne, gagnant);
                        break;
                    }
                }
            }
        }
    } 
}

/* Auteur : Mathis */
/* Date :  17/06/2024 */
/* Résumé : Fonction qui permet de savoir si un joueur peut jouer */
/* Entrée(s) : La liste de tous les pions, le pion dont on veut savoir s'il peut jouer et la valeur du dé  */
/* Sortie(s) : 1 si le pion peut jouer, 0 sinon  */
int PeutJouer(Pion** tab_allpions, Pion pion, int valeur_de)
{
    if (pion.num_case>=0)
    {
        for (int i=pion.num_case; i<pion.num_case+valeur_de; i++)   
        {
            if (VerifBarrage(i+1, tab_allpions))
            {
                return 0;
            }
        }
    }   
    return (((pion.nb_case_franchies+valeur_de < 73) && (pion.num_case != -pion.couleur)) || ((valeur_de == 6) && (pion.num_case == -pion.couleur)));
}

/* Auteur : Dorian */
/* Date :  12/06/2024 */
/* Résumé : Fonction qui renvoie le nombre de pions sur une case donnée */
/* Entrée(s) : Une case et le tableau de tous les pions  */
/* Sortie(s) : Nombre de pions sur la case donnée  */
int CompterPionsCase(int caze, Pion** tab_allpions)
{
    int taille=0;
    for (int i=0; i<4; i++)
    {
        for (int j=0; j<4; j++)
        {
            if (tab_allpions[i][j].num_case == caze)
            {
                taille++;
            }
        }
    }
    return taille;
}

/* Auteur : Dorian */
/* Date :  12/06/2024 */
/* Résumé : Fonction qui renvoie la liste de pions présents sur une case */
/* Entrée(s) : Une case et le tableau de tous les pions  */
/* Sortie(s) : Liste des pions sur la case  */
Pion* PionsSurCaze(int caze, Pion** tab_allpions)
{
    Pion* liste_pions_case = malloc(16*sizeof(Pion));
    int k=0;
    for (int i=0; i<4; i++)
    {
        for (int j=0; j<4; j++)
        {
            if (tab_allpions[i][j].num_case == caze)
            {
                liste_pions_case[k] = tab_allpions[i][j];
                k++;
            }
        }
    }
    return liste_pions_case;
}

/* Auteur : Dorian */
/* Date :  12/06/2024 */
/* Résumé : Procédure qui gère le numéro des cases qui sont à l'intérieur du plateau (bleu = 69,70,...76 ; jaune = 169,170,...,176 ; etc ) */
/* Entrée(s) : Un pion  */
void GererCasesFin(Pion* tab_pions, int num_pion)
{
    {
        switch (tab_pions[num_pion].couleur)
        {
            case 0:
            tab_pions[num_pion].num_case = tab_pions[num_pion].nb_case_franchies + 4;
            break;
            case 1:
            tab_pions[num_pion].num_case = tab_pions[num_pion].nb_case_franchies + 104;
            break;
            case 2:
            tab_pions[num_pion].num_case = tab_pions[num_pion].nb_case_franchies + 204;
            break;
            case 3:
            tab_pions[num_pion].num_case = tab_pions[num_pion].nb_case_franchies + 304;
            break;
        }
    }
}

/* Auteur : Dorian */
/* Date :  01/06/2024 */
/* Résumé : Procédure qui permet au joueur de jouer, manger, mettre à jour les cases */
/* Entrée(s) : Liste des joueurs de la partie, le joueur qui doit jouer, la valeur du dé qu'il a tiré et la liste de tous les pions  */
void Jouer(Joueur* tab_j, Joueur j, int valeur_de, Pion** tab_allpions, int num_j, int* a_gagne, int* gagnant)
{
    int num_pion;
    char rep;
    int caze;
    int i=0;
    int tmp;
    Pion* liste_pions_case;
    Pion* liste_pions_case2;
    do
    {
        num_pion = choixPion(); 
        i++;
    } while ((!PeutJouer(tab_allpions, j.tab_pions[num_pion], valeur_de)) && (i<5)); // vérifie si chaque pion donné par l'utilisateur est jouable ou non
    if (i == 5)
    {
        printf("Vous ne pouvez pas jouer, vous passez votre tour\n");
    }else
    {
        if ((valeur_de == 6) && (j.tab_pions[num_pion].num_case == -j.couleur)) // si le pion est à la maison
        {
            caze = CazeDepart(j);
            tmp = 1;
            tab_j[num_j].nb_pions_sortis++;
        }else
        {
            tmp = valeur_de;
            caze = j.tab_pions[num_pion].num_case + valeur_de; // variable temporaire pour voir si la case est disponible ou s'il y a un barrage
        }
        if (j.aBarrage)
        {
            int* pion_formant_barrage = malloc(2*sizeof(int));
            pion_formant_barrage = PionEnBarrage(j);
            for (int a=0; a<2; a++)
            {
                if (num_pion == pion_formant_barrage[a]+1)
                {
                    tab_j[num_j].aBarrage=0;
                }
            }
        }
        if (cazeOccupe(tab_allpions, caze) && !CazeRefuge(caze) && j.tab_pions[num_pion].nb_case_franchies+valeur_de<64) // si la case est occupée et que ce n'est pas une case refuge et que le barrage n'est pas sur une case où le pion ne doit pas aller / sur la flèche
        {
            int taille = CompterPionsCase(caze, tab_allpions);
            liste_pions_case = malloc(taille*sizeof(Pion));
            liste_pions_case = PionsSurCaze(caze, tab_allpions);
            if (!PionsMemeCouleur(j.tab_pions[num_pion], taille, liste_pions_case)) 
            {
                printf("Voulez-vous manger ? (y/n)\n");
                scanf("%c", &rep);
                if (rep == 'y')
                {
                    printf("Vous avez mangé !\n");
                    for (int m=0; m<taille; m++)         // triple boucle pour vérifier quels pions manger
                    {
                        for (int k=0; k<4; k++)
                        {
                            for (int n=0; n<4; n++)
                            {
                                if ((tab_j[k].couleur == liste_pions_case[m].couleur) && (tab_j[k].tab_pions[n].num_case == caze))
                                {
                                    tab_j[k].nb_pions_sortis--;
                                    tab_j[k].tab_pions[n].num_case = -tab_j[k].couleur;     // change les informations induites par la regle
                                    tab_j[k].tab_pions[n].nb_case_franchies = 0;
                                }
                            }
                        }
                    }
                }
            }
        }
        int taille2 = CompterPionsCase(caze, tab_allpions);
        liste_pions_case2 = malloc(taille2*sizeof(Pion));
        liste_pions_case2 = PionsSurCaze(caze, tab_allpions);
        if ((tab_j[num_j].tab_pions[num_pion].nb_case_franchies < 64) && cazeOccupe(tab_allpions, caze) && PionsMemeCouleur(j.tab_pions[num_pion], taille2, liste_pions_case2))
        {
            tab_j[num_j].aBarrage =1;          //si les pion sont de la même couleur alors on créé un barrage
        }
        if (caze == 68)
        {
            tab_j[num_j].tab_pions[num_pion].num_case = 68;
        }else
        {
            tab_j[num_j].tab_pions[num_pion].num_case = caze%68;
        }
        tab_j[num_j].tab_pions[num_pion].nb_case_franchies = j.tab_pions[num_pion].nb_case_franchies + tmp;
        tab_j[num_j].dernier_pion_joue = j.tab_pions[num_pion];
        tab_j[num_j].num_dernier_pion_joue = num_pion;
        if (tab_j[num_j].tab_pions[num_pion].nb_case_franchies > 64)
        {
            GererCasesFin(tab_j[num_j].tab_pions, num_pion); 
        }
        printf("A barrage ? : %d\n", tab_j[num_j].aBarrage);
        if (PionArrive(tab_j[num_j].tab_pions[num_pion])) // pion arrive renvoie un int
        {
            printf("Votre pion %d est arrivé !\n", num_pion+1);
            tab_j[num_j].nb_pions_arrives = j.nb_pions_arrives + 1;
            tab_j[num_j].tab_pions[num_pion].num_case = -4;
            JoueurGagne(tab_j[num_j], a_gagne);
            if (*a_gagne!=0)
            {
                *gagnant=num_j+1;
            }
        }
    }
}

/* Auteur : Dorian */
/* Date :  15/06/2024 */
/* Résumé : Procédure qui fait jouer le robot automatiquement (il jouera d'abord le pion 1 s'il peut, sinon le 2, sinon le 3, sinon le 4) */
/* Entrée(s) : Liste des joueurs de la partie, le joueur qui doit jouer, la valeur du dé qu'il a tiré et la liste de tous les pions  */
void JouerRobot(Joueur* tab_j, Joueur j, int valeur_de, Pion** tab_allpions, int num_j, int* a_gagne, int* gagnant)
{
    int num_pion;
    int caze;
    int i=-1;
    int k=0;
    int tmp;
    int bonus=0;
    Pion* liste_pions_case;
    while (i == -1)
    {
        if (PeutJouer(tab_allpions, j.tab_pions[k], valeur_de) && i<3)
        {
            i=k;
        }
        k++;
    }
    if (i == -1 || i >= 4)
    {
        printf("Le robot ne peut pas jouer, il passe son tour\n");
    }else
    {
        num_pion = i;
        if ((valeur_de == 6) && (j.tab_pions[num_pion].num_case == -j.couleur))
        {
            caze = CazeDepart(j);
            tmp = 1;
            tab_j[num_j].nb_pions_sortis++;
        }else
        {
            tmp = valeur_de;
            caze = j.tab_pions[num_pion].num_case + valeur_de;
        }
        if (j.aBarrage)
        {
            int* num_pion_formant_barrage = malloc(2*sizeof(int));
            num_pion_formant_barrage = PionEnBarrage(j);
            for (int a=0; a<2; a++)
            {
                if (num_pion == num_pion_formant_barrage[a])
                {
                    tab_j[num_j].aBarrage=0;
                }
            }
            free(num_pion_formant_barrage);
        }
        if ((cazeOccupe(tab_allpions, caze) && !CazeRefuge(caze)) || (cazeOccupe(tab_allpions, caze) && j.tab_pions[num_pion].num_case == -j.couleur))
        {
            int taille = CompterPionsCase(caze, tab_allpions);
            liste_pions_case = malloc(taille*sizeof(Pion));
            liste_pions_case = PionsSurCaze(caze, tab_allpions);
            if (!PionsMemeCouleur(j.tab_pions[num_pion], taille, liste_pions_case))
            {
                printf("Le robot a mangé un pion !\n");
                for (int m=0; m<taille; m++)         // triple boucle pour vérifier quels pions manger
                {
                    for (int l=0; l<4; l++)
                    {
                        for (int n=0; n<4; n++)
                        {
                            if (l==2)
                            {
                            }
                            if ((tab_j[l].couleur == liste_pions_case[m].couleur) && (tab_j[l].tab_pions[n].num_case == caze))
                            {
                                tab_j[l].nb_pions_sortis--;
                                tab_j[l].tab_pions[n].num_case = -tab_j[l].couleur;     // change les informations induites par la regle
                                tab_j[l].tab_pions[n].nb_case_franchies = 0;
                            }
                        }
                    }
                }
                if (valeur_de==6 && caze==CazeDepart(j))
                {
                    bonus=1;
                }
            }else{
                if (tab_j[num_j].tab_pions[num_pion].nb_case_franchies < 64)
                {
                    tab_j[num_j].aBarrage =1;          //si les pion sont de la même couleur alors on créé un barrage
                }
            }
        }
        if (caze == 68)
        {
            tab_j[num_j].tab_pions[num_pion].num_case = 68;
            tab_j[num_j].tab_pions[num_pion].nb_case_franchies = j.tab_pions[num_pion].nb_case_franchies + tmp;
        }else
        {
            if (bonus)
            {
                tab_j[num_j].tab_pions[num_pion].num_case = caze;
                if (PeutJouer(tab_allpions, tab_j[num_j].tab_pions[num_pion], 20))
                {
                    printf("Le robot a un bonus de 20 cases pour avoir mangé un pion adverse !\n");
                    tab_j[num_j].tab_pions[num_pion].num_case = (20 + caze)%68;
                    tab_j[num_j].tab_pions[num_pion].nb_case_franchies = 21;
                }else
                {
                    printf("Un barrage bloque l'avancée du robot, son bonus de 20 cases est annulé\n");
                }
            }else
            {
                tab_j[num_j].tab_pions[num_pion].nb_case_franchies = j.tab_pions[num_pion].nb_case_franchies + tmp;
                tab_j[num_j].tab_pions[num_pion].num_case = caze%68;
            }
        }
        tab_j[num_j].dernier_pion_joue = j.tab_pions[num_pion];
        tab_j[num_j].num_dernier_pion_joue = num_pion;
        if (j.tab_pions[num_pion].nb_case_franchies > 64)
        {
            GererCasesFin(tab_j[num_j].tab_pions, num_pion); 
        }
        if (PionArrive(j.tab_pions[num_pion])) // pion arrive renvoie un int
        {
            printf("Le pion %d du robot est arrivé !\n", num_pion+1);
            tab_j[num_j].nb_pions_arrives = j.nb_pions_arrives + 1;
            tab_j[num_j].tab_pions[num_pion].num_case = -4;
            JoueurGagne(tab_j[num_j], a_gagne);
            if (*a_gagne!=0)
            {
                *gagnant=num_j+1;
            }
        }
    }
}


