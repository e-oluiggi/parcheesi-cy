#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "jeu.h"
#include "regle.h"

// fonction qui permet de connaitre la case de départ de chaque joueur
int CazeDepart(Joueur j)
{
    switch (j.couleur)
    {
    case 0:
    return 22;
    case 1:
    return 5;
    case 2:
    return 56;
    case 3:
    return 39;
    default:
    return 0;
    }
}

// fonction qui renvoie 1 si la case donnée est une case refuse, 0 sinon
int CazeRefuge(int caze)
{
    return (caze == 5 || caze == 12 || caze == 17 || caze == 22 || caze == 29 || caze == 34 || caze == 39 || caze == 46 || caze == 51 || caze == 56 || caze == 63 || caze == 68);
}

// fonction qui permet de determiner la case d'arrivée de chaque joueur en fonction de leur couleur  
int PionArrive(Pion Pion)           
{
    switch (Pion.couleur)
    {
        case 0:
        return Pion.num_case==76;
        case 1:
        return Pion.num_case==176;
        case 2:
        return Pion.num_case==276;
        case 3:
        return Pion.num_case==376;
        default:
        return 0;
    }    
}

/* Auteur : Dorian */
/* Date :  15/06/2024 */
/* Résumé : procédure qui permet de savoir si le joueur a gagné */
/* Entrée(s) : Un joueur  */
void JoueurGagne(Joueur j, int* a_gagne)
{
    *a_gagne = (j.nb_pions_arrives == 4); // variable condition de la boucle while principale
}


// fonction qui permet de savoir si 2 pions sur la meme case sont de la meme couleur.    
int PionsMemeCouleur(Pion pion, int N, Pion* tab_pions_case){     // N étant le nombre de pions sur la caze en question
    int memeCouleur=0;
    for(int i=0; i<N;i++){
        if (tab_pions_case[i].couleur==pion.couleur){
            memeCouleur++;
        }  
    }
    return memeCouleur>0;
}



// fonction qui permet de savoir si la case de depart est occupé par un adversaire
int MaisonOccup(Pion** tab_allpions, Joueur J)
{
    int MaisonOccup=0;
    for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
            if((tab_allpions[i][j].couleur != J.couleur) && (tab_allpions[i][j].num_case==CazeDepart(J))){
                MaisonOccup++;
            }
        }
    }
    return MaisonOccup!=0;
}


// fonction qui permet de savoir si la case est occupée par un pion
int cazeOccupe(Pion** tab_allpions, int caze)
{
    int compteur=0;
    for (int i=0; i<4;i++)
    {
        for(int j=0; j<4; j++)
        {
            if(tab_allpions[i][j].num_case==caze)
            {
                compteur++;
            }
                
        }
    }
    return compteur!=0;
}

// fonction permettant de savoir le numero de la case ou le joueur j a un barrage de sa couleur
int* PionEnBarrage(Joueur j)
{
    int* pionBarrage = malloc(2*sizeof(int));
    int indice=0;
    if (j.aBarrage)
    {   
        for (int i=0; i<4; i++){
            for (int k=i+1; k<4; k++)
            {
                if (j.tab_pions[i].num_case==j.tab_pions[k].num_case)
                {
                    pionBarrage[indice]=i;
                    indice++;
                }
              
            }  
        }
    }
    return pionBarrage;
}

// fonction permettant de savoir s'il y a un barrage sur la case en question
int VerifBarrage(int caze, Pion** tab_allpions){
    int compteur =0;
    for(int i=0; i<4; i++)
    {
        for(int j=0; j<4; j++)
        {
            for(int k=j+1; k<4; k++)
            {
                if((tab_allpions[i][j].num_case == caze) && (tab_allpions[i][j].couleur==tab_allpions[i][k].couleur) && (tab_allpions[i][k].num_case==tab_allpions[i][j].num_case) && (caze > 0))
                {
                    compteur++;
                }
            }
        }
    }
    return compteur != 0;
}




       

