#ifndef __aff_H_
#define __aff_H_
#include "jeu.h"
#include "regle.h"


void affichagede(int nb);
int** allouer();
void initialiser(int** plateau);
void affPlateau(int** plateau, Pion** tab_allpions);
void afficherPionsJoueur(Joueur j);
void affPanier(Pion** tab_allpions);
#endif