#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include "jeu.h"
#include "aff.h"
#include "regle.h"
#include "unistd.h"

#define COLOR_ROUGE  "\e[38;2;220;0;0m" /*Les 3 derniers chiffre donne le code rgb, rrr,ggg,bbb   \
                       (red,green,blue) */
#define COLOR_VERT "\e[38;2;0;180;0m"
#define COLOR_JAUNE "\e[38;2;200;200;0m"
#define COLOR_BLEU "\e[38;2;0;150;255m"

#define COLOR_RESET "\x1b[0m"



int main()
{
    srand(time(NULL));
    int aGagnervaleur = 0;
    int* a_gagne = &aGagnervaleur;
    int* gagnant = &aGagnervaleur;
    int valeur_de;
    Joueur* tab_joueurs;
    Pion** tab_allpions = malloc(4*sizeof(Pion*));
    char* tab_rep = malloc(4*sizeof(char));
    tab_rep = HumainOuOrdi(4);
    int** plateau = allouer();
    initialiser(plateau);
    tab_joueurs = InitTabJoueurs(tab_rep);
    for (int i=0; i<4; i++)
    {
        tab_allpions[i] = malloc(4*sizeof(Pion));
        tab_allpions[i] = tab_joueurs[i].tab_pions;
    }
    while (!(*a_gagne))
    {
        for (int j=0; j<4; j++)
        {
            valeur_de = lancerD();
            switch(j){
                case 0:
                    printf(COLOR_BLEU "Joueur %d " COLOR_RESET ": \n", j+1);
                    affichagede(valeur_de);
                    printf("Vous avez tiré un %d !\n", valeur_de);
                    break;
                case 1:
                    printf(COLOR_JAUNE "Joueur %d " COLOR_RESET ": \n", j+1);
                    affichagede(valeur_de);
                    printf("Vous avez tiré un %d !\n", valeur_de);
                    break;
                case 2:
                    printf(COLOR_VERT "Joueur %d " COLOR_RESET ": \n", j+1);
                    affichagede(valeur_de);
                    printf("Vous avez tiré un %d !\n", valeur_de);
                    break;
                case 3:
                    printf(COLOR_ROUGE "Joueur %d " COLOR_RESET ": \n", j+1);
                    affichagede(valeur_de);
                    printf("Vous avez tiré un %d !\n", valeur_de);
                    break; 
            }
            if (valeur_de == 6)
            {
                tab_joueurs[j].compteur_six_daffilee++;
                tab_joueurs[j].compteur_six++;
                sleep(1);
                afficherPionsJoueur(tab_joueurs[j]);
                GererReglesSix(tab_joueurs, tab_joueurs[j], tab_allpions, j, a_gagne, gagnant);
                printf("affichage du plateau de jeu : \n");
                affPlateau(plateau, tab_allpions);
            }else
            {
                afficherPionsJoueur(tab_joueurs[j]);
                tab_joueurs[j].compteur_six_daffilee = 0;
                sleep(1);
                if (tab_joueurs[j].compteur_six == 0 || (tab_joueurs[j].nb_pions_sortis==0 && tab_joueurs[j].compteur_six>1))
                {
                    printf("Le joueur %d ne peut pas jouer, il passe son tour\n",j+1);
                }else
                {
                    if (tab_joueurs[j].humain)
                    {
                        Jouer(tab_joueurs, tab_joueurs[j], valeur_de, tab_allpions, j, a_gagne, gagnant);
                    }else                                                                 
                    {
                        JouerRobot(tab_joueurs, tab_joueurs[j], valeur_de, tab_allpions, j, a_gagne, gagnant);
                    }
                    printf("affichage du plateau de jeu : \n");
                    affPlateau(plateau, tab_allpions);
                }
            }
            sleep(2); // affichage du plateau pendant 2s
            printf("Au joueur suivant de jouer ! \n");
        }
    }
    printf("Fin du tour\nLe gagnant est : le joueur %d !\n", *gagnant);
    free(tab_rep);
    for(int i=0; i<4; i++){
        free(tab_allpions[i]);
    }
    free(tab_allpions);
    for(int i = 0; i<19; i++){
        free(plateau[i]);
    }
    free(plateau);
    return 0;
}
