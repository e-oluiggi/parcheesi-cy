#ifndef __regle_H_
#define __regle_H_


void JoueurGagne(Joueur J, int* a_gagne);
int PionArrive(Pion Pion);
int CazeDepart(Joueur j);
int CazeRefuge(int caze);
int PionsMemeCouleur(Pion pion, int N, Pion* tab_pions_case);
int MaisonOccup(Pion** tab_allpions, Joueur J);
int cazeOccupe(Pion** tab_allpions, int caze);
int* PionEnBarrage(Joueur j);
int VerifBarrage(int caze, Pion** tab_allpions);
#endif