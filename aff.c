#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "aff.h"
#include "jeu.h"


/*Sert a mettre de la couleur dans la console, les couleurs sont variable d'une
* console a une autre, à adapter*/
/*On definit des variable fixe pour faciliter l'application de couleur*/
#define COLOR_ROUGE  "\e[38;2;220;0;0m" /*Les 3 derniers chiffre donne le code rgb, rrr,ggg,bbb   \
                       (red,green,blue) */
#define COLOR_VERT "\e[38;2;0;180;0m"
#define COLOR_JAUNE "\e[38;2;200;200;0m"
#define COLOR_BLEU "\e[38;2;0;150;255m"

#define COLOR_JAUNE_BG "\x1b[43m" /*Code couleur prédéfinit*/
#define COLOR_ROUGE_BG "\x1b[41m" 
#define COLOR_VERT_BG "\x1b[42m" 
#define COLOR_BLEU_BG "\x1b[44m"


#define COLOR_BLANC_BG "\x1b[47m"

#define COLOR_BLANC "\e[38;2;255;255;255m"
#define COLOR_NOIR "\e[38;2;0;0;0m"
#define COLOR_RESET "\x1b[0m"

/* Auteur : Oscar et Moatez */
/* Entrée(s) : nb l'entier tiré par la fonction lancerD  */

 

void affichagede(int nb){
    switch(nb){
        case 1:
            printf(COLOR_BLANC_BG "           " COLOR_RESET "\n");
            printf(COLOR_BLANC_BG "           " COLOR_RESET "\n" );
            printf(COLOR_BLANC_BG "     •     " COLOR_RESET "\n");
            printf(COLOR_BLANC_BG "           " COLOR_RESET "\n");
            printf(COLOR_BLANC_BG "           " COLOR_RESET "\n");
            break;
        case 2:
            printf(COLOR_BLANC_BG "           " COLOR_RESET "\n");
            printf(COLOR_BLANC_BG "        •  " COLOR_RESET "\n" );
            printf(COLOR_BLANC_BG "           " COLOR_RESET "\n");
            printf(COLOR_BLANC_BG "  •        " COLOR_RESET "\n");
            printf(COLOR_BLANC_BG "           " COLOR_RESET "\n");
            break;
        case 3:
            printf(COLOR_BLANC_BG "           " COLOR_RESET "\n");
            printf(COLOR_BLANC_BG "        •  " COLOR_RESET "\n" );
            printf(COLOR_BLANC_BG "     •     " COLOR_RESET "\n");
            printf(COLOR_BLANC_BG "  •        " COLOR_RESET "\n");
            printf(COLOR_BLANC_BG "           " COLOR_RESET "\n");
            break;
        case 4:
            printf(COLOR_BLANC_BG "           " COLOR_RESET "\n");
            printf(COLOR_BLANC_BG "  •     •  " COLOR_RESET "\n" );
            printf(COLOR_BLANC_BG "           " COLOR_RESET "\n");
            printf(COLOR_BLANC_BG "  •     •  " COLOR_RESET "\n");
            printf(COLOR_BLANC_BG "           " COLOR_RESET "\n");
            break;
        case 5:
            printf(COLOR_BLANC_BG "           " COLOR_RESET "\n");
            printf(COLOR_BLANC_BG "  •     •  " COLOR_RESET "\n" );
            printf(COLOR_BLANC_BG "     •     " COLOR_RESET "\n");
            printf(COLOR_BLANC_BG "  •     •  " COLOR_RESET "\n");
            printf(COLOR_BLANC_BG "           " COLOR_RESET "\n");
            break;
        case 6:
            printf(COLOR_BLANC_BG "           " COLOR_RESET "\n");
            printf(COLOR_BLANC_BG "  •     •  " COLOR_RESET "\n" );
            printf(COLOR_BLANC_BG "  •     •  " COLOR_RESET "\n");
            printf(COLOR_BLANC_BG "  •     •  " COLOR_RESET "\n");
            printf(COLOR_BLANC_BG "           " COLOR_RESET "\n");
            break;
    }

}




/* Auteur : Oscar et Moatez */
/* Sortie(s) : retourne un plateau vide avec la taille qui convient */

int** allouer(void){
    int** plateau;
    plateau = malloc(19*sizeof(int*));
    for(int i=0; i < 19; i++){
        
        plateau[i] = malloc(18*sizeof(int));
    }
    return plateau;
}

/* Auteur : Oscar et Moatez */
/* Entrée(s) : le plateau de jeu alloué  */


void initialiser(int** plateau){

    //première boucle initialise les valeurs pour le premier bloc avec le jaune et vert
    for(int i=0;i<8;i++){    
        for(int j=0;j<7;j++){
            plateau[i][j] = -6;     //créer le bloc jaune (fond jaune = -6)
        }
        plateau[i][7] = 1+i;
        plateau[i][9] = 67-i;
        for(int j=10;j<17;j++){
            plateau[i][j] = -7;     //créer le bloc vert (fond vert = -7)
        }
        
    }

    plateau[0][8] = 68;
    for(int i=1;i<9;i++){
        plateau[i][8] = 168 + i; //On fait la flèche finale jaune (le "1" represente le jaune)
    }

    //intitalisation du bloc du milieu
    
    for(int j=0;j<8;j++){
        plateau[8][j] = 16 - j;
        plateau[10][j] = 18 + j;
    }

    plateau[9][0] = 17;
    for(int j=1; j<9; j++){
        plateau[9][j] = 68 + j; //On fait la flèche finale bleu (le "0" represente le bleu)
    }

    
    for(int i=0; i<19; i++){
        plateau[i][17] = -4; //dèrnière colonne qui est vidsauf la case ou il y a 51
    }

    plateau[9][17] = 51;

    for(int j=9; j<17; j++){
        plateau[9][j] = 276 - j + 9; //On fait la flèche finale verte (le "2" represente le vert)
    }
    
    for(int j=9; j<17; j++){
        plateau[8][j] = 59 - j + 9;
        plateau[10][j] = 43 + j -9;
    }

    //Deuxième boucle initialise les valeurs pour le troisième bloc avec le bleu et rouge
    for(int i=11;i<19;i++){    
        for(int j=0;j<7;j++){
            plateau[i][j] = -5;     //créer le bloc bleu (fond bleu = -5)
        }
        plateau[i][7] = 26+i-11;
        plateau[i][9] = 42-i+11;
        for(int j=10;j<17;j++){
            plateau[i][j] = -8;     //créer le bloc rouge (fond rouge = -8)
        }
    }

    plateau[18][8] = 34;
    for(int i=10;i<18;i++){
        plateau[i][8] = 375 - i +10; //On fait la flèche finale rouge (le "3" represente le rouge)
    }
}



/* Auteur : Oscar et Moatez */
/* Entrée(s) : le plateau de jeu et le tableau qui contient tout les pions  */

void affPlateau(int** plateau, Pion** tab_allpions){
    int nbrPion;                                      
    for(int i=0;i<19;i++){
        for(int j=0;j<18;j++){
            nbrPion = CompterPionsCase(plateau[i][j], tab_allpions); //on obtien le nombre de pion sur la case
            switch(nbrPion){
                case 0:
                    if(plateau[i][j] < 69){                   // le code qui s'execute quand on a aucun pion sur la case
                        switch(plateau[i][j]){                            
                            case -5:
                                printf(COLOR_BLEU_BG "    " COLOR_RESET);
                                break;
                            case -6:
                                printf(COLOR_JAUNE_BG "    " COLOR_RESET);
                                break;
                            case -7:
                                printf(COLOR_VERT_BG "    " COLOR_RESET);
                                break;
                            case -8:
                                printf(COLOR_ROUGE_BG "    " COLOR_RESET);
                                break;
                            case -4:
                                printf("");
                                break;
                            case 68:
                                printf(COLOR_BLANC_BG "        " COLOR_RESET);
                                break;
                            case 34:
                                printf(COLOR_BLANC_BG "        " COLOR_RESET);
                                break;
                            case 12:
                                printf(COLOR_BLANC_BG "    " COLOR_RESET);
                                break;
                            case 46:
                                printf(COLOR_BLANC_BG "    " COLOR_RESET);
                                break;
                            case 51:
                                printf(COLOR_BLANC_BG "    " COLOR_RESET);
                                break;
                            case 63:
                                printf(COLOR_BLANC_BG "    " COLOR_RESET);
                                break;
                            case 17:
                                printf(COLOR_BLANC_BG "    " COLOR_RESET);
                                break;
                            case 29:
                                printf(COLOR_BLANC_BG "    " COLOR_RESET);
                                break;
                            case 22:
                                printf(COLOR_BLEU_BG "    " COLOR_RESET);
                                break;
                            case 39:
                                printf(COLOR_ROUGE_BG "    " COLOR_RESET);
                                break;
                            case 56:
                                printf(COLOR_VERT_BG "    " COLOR_RESET);
                                break;
                            case 5:
                                printf(COLOR_JAUNE_BG "    " COLOR_RESET);
                                break;
                            default:
                                if(plateau[i][j] > 9){
                                    printf(" %d ", plateau[i][j]);
                                }else {
                                    printf(" %d  ", plateau[i][j]);
                                }
                                
                                break;
                        }
                    }else if(plateau[i][j] < 100){                              //code pour les flèches
                        printf(COLOR_BLEU_BG "    " COLOR_RESET);

                    }else if(plateau[i][j] < 200){
                        printf(COLOR_JAUNE_BG "        " COLOR_RESET );

                    }else if(plateau[i][j] < 300){
                        printf(COLOR_VERT_BG "    " COLOR_RESET);

                    }else if(plateau[i][j] < 400){
                        printf(COLOR_ROUGE_BG "        " COLOR_RESET );

                    }
                    break;
                case 1:                                                             // cas ou il y a un seul pion
                    for(int k=0; k<4;k++){                  
                        for(int l=0; l<4; l++){
                            if(plateau[i][j] == tab_allpions[k][l].num_case){  
                                switch(tab_allpions[k][l].couleur){                 //on va faire les cas par couleurs
                                    case 0:                                         // 0 = bleu
                                        if(plateau[i][j] < 69){ 
                                            switch(plateau[i][j]){                     //on doit distinguer tout les cas pour les cases refuges
                                                case 68:                            
                                                    printf(COLOR_BLANC_BG "    " COLOR_BLEU "▣" COLOR_RESET COLOR_BLANC_BG "   " COLOR_RESET);
                                                    break;
                                                case 34:
                                                    printf(COLOR_BLANC_BG "    " COLOR_BLEU "▣" COLOR_RESET COLOR_BLANC_BG "   " COLOR_RESET);
                                                    break;
                                                case 12:
                                                    printf(COLOR_BLANC_BG "  " COLOR_BLEU "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 46:
                                                    printf(COLOR_BLANC_BG "  " COLOR_BLEU "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 51:
                                                    printf(COLOR_BLANC_BG "  " COLOR_BLEU "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 63:
                                                    printf(COLOR_BLANC_BG "  " COLOR_BLEU "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 17:
                                                    printf(COLOR_BLANC_BG "  " COLOR_BLEU "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 29:
                                                    printf(COLOR_BLANC_BG "  " COLOR_BLEU "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case -4:
                                                    printf("");
                                                    break;
                                                case 22:
                                                    printf(COLOR_BLEU_BG COLOR_BLEU"  ▣ " COLOR_RESET);
                                                    break;
                                                case 39:
                                                    printf(COLOR_ROUGE_BG COLOR_BLEU"  ▣ " COLOR_RESET);
                                                    break;
                                                case 56:
                                                    printf(COLOR_VERT_BG COLOR_BLEU "  ▣ " COLOR_RESET);
                                                    break;
                                                case 5:
                                                    printf(COLOR_JAUNE_BG COLOR_BLEU"  ▣ " COLOR_RESET);
                                                    break;
                                                default:
                                                    printf(COLOR_BLEU COLOR_BLEU"  ▣ " COLOR_RESET);
                                                    break;
                                            }
                                        }else if(plateau[i][j] < 100){                              //code pour les flèches
                                            printf(COLOR_BLEU_BG "  " COLOR_BLEU "▣" COLOR_BLEU_BG " " COLOR_RESET);
                                        }
                                        break;
                                    case 1:                                 // 1 = jaune
                                        if(plateau[i][j] < 69){
                                            switch(plateau[i][j]){
                                                case 68:
                                                    printf(COLOR_BLANC_BG "    " COLOR_JAUNE "▣" COLOR_RESET COLOR_BLANC_BG "   " COLOR_RESET);
                                                    break;
                                                case 34:
                                                    printf(COLOR_BLANC_BG "    " COLOR_JAUNE "▣" COLOR_RESET COLOR_BLANC_BG "   " COLOR_RESET);
                                                    break;
                                                case 12:
                                                    printf(COLOR_BLANC_BG "  " COLOR_JAUNE "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 46:
                                                    printf(COLOR_BLANC_BG "  " COLOR_JAUNE "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 51:
                                                    printf(COLOR_BLANC_BG "  " COLOR_JAUNE "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 63:
                                                    printf(COLOR_BLANC_BG "  " COLOR_JAUNE "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 17:
                                                    printf(COLOR_BLANC_BG "  " COLOR_JAUNE "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 29:
                                                    printf(COLOR_BLANC_BG "  " COLOR_JAUNE "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case -4:
                                                    printf("");
                                                    break;
                                                case 22:
                                                    printf(COLOR_BLEU_BG COLOR_JAUNE"  ▣ " COLOR_RESET);
                                                    break;
                                                case 39:
                                                    printf(COLOR_ROUGE_BG COLOR_JAUNE"  ▣ " COLOR_RESET);
                                                    break;
                                                case 56:
                                                    printf(COLOR_VERT_BG COLOR_JAUNE "  ▣ " COLOR_RESET);
                                                    break;
                                                case 5:
                                                    printf(COLOR_JAUNE_BG COLOR_JAUNE"  ▣ " COLOR_RESET);
                                                    break;
                                                default:
                                                    printf(COLOR_JAUNE "  ▣ " COLOR_RESET);
                                                    break;
                                            }

                                        }else if(plateau[i][j] < 200){
                                            printf(COLOR_JAUNE_BG "    " COLOR_JAUNE "▣" COLOR_JAUNE_BG "   " COLOR_RESET );
                                        }
                                        break;
                                    case 2:                                     //2 = Vert
                                        if(plateau[i][j] < 69){
                                            switch(plateau[i][j]){
                                                case 68:
                                                    printf(COLOR_BLANC_BG "    " COLOR_VERT "▣" COLOR_RESET COLOR_BLANC_BG "   " COLOR_RESET);
                                                    break;
                                                case 34:
                                                    printf(COLOR_BLANC_BG "    " COLOR_VERT "▣" COLOR_RESET COLOR_BLANC_BG "   " COLOR_RESET);
                                                    break;
                                                case 12:
                                                    printf(COLOR_BLANC_BG "  " COLOR_VERT "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 46:
                                                    printf(COLOR_BLANC_BG "  " COLOR_VERT "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 51:
                                                    printf(COLOR_BLANC_BG "  " COLOR_VERT "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 63:
                                                    printf(COLOR_BLANC_BG "  " COLOR_VERT "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 17:
                                                    printf(COLOR_BLANC_BG "  " COLOR_VERT "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 29:
                                                    printf(COLOR_BLANC_BG "  " COLOR_VERT "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case -4:
                                                    printf("");
                                                    break;
                                                case 22:
                                                    printf(COLOR_BLEU_BG COLOR_VERT"  ▣ " COLOR_RESET);
                                                    break;
                                                case 39:
                                                    printf(COLOR_ROUGE_BG COLOR_VERT"  ▣ " COLOR_RESET);
                                                    break;
                                                case 56:
                                                    printf(COLOR_VERT_BG COLOR_VERT "  ▣ " COLOR_RESET);
                                                    break;
                                                case 5:
                                                    printf(COLOR_JAUNE_BG COLOR_VERT"  ▣ " COLOR_RESET);
                                                    break;
                                                default:
                                                    printf(COLOR_VERT "  ▣ " COLOR_RESET);
                                                    break;
                                            }
                                        }else if(plateau[i][j] < 300){
                                            printf(COLOR_VERT_BG "  " COLOR_VERT "▣" COLOR_VERT_BG " " COLOR_RESET);
                                        }
                                        break;
                                    case 3:                                     //3 = rouge
                                        if(plateau[i][j] < 69){    
                                            switch(plateau[i][j]){
                                                case 68:
                                                    printf(COLOR_BLANC_BG "    " COLOR_ROUGE "▣" COLOR_RESET COLOR_BLANC_BG "   " COLOR_RESET);
                                                    break;
                                                case 34:
                                                    printf(COLOR_BLANC_BG "    " COLOR_ROUGE "▣" COLOR_RESET COLOR_BLANC_BG "   " COLOR_RESET);
                                                    break;
                                                case 12:
                                                    printf(COLOR_BLANC_BG "  " COLOR_ROUGE "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 46:
                                                    printf(COLOR_BLANC_BG "  " COLOR_ROUGE "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 51:
                                                    printf(COLOR_BLANC_BG "  " COLOR_ROUGE "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 63:
                                                    printf(COLOR_BLANC_BG "  " COLOR_ROUGE "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 17:
                                                    printf(COLOR_BLANC_BG "  " COLOR_ROUGE "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case 29:
                                                    printf(COLOR_BLANC_BG "  " COLOR_ROUGE "▣" COLOR_RESET COLOR_BLANC_BG " " COLOR_RESET);
                                                    break;
                                                case -4:
                                                    printf("");
                                                    break;
                                                case 22:
                                                    printf(COLOR_BLEU_BG COLOR_ROUGE"  ▣ " COLOR_RESET);
                                                    break;
                                                case 39:
                                                    printf(COLOR_ROUGE_BG COLOR_ROUGE"  ▣ " COLOR_RESET);
                                                    break;
                                                case 56:
                                                    printf(COLOR_VERT_BG COLOR_ROUGE "  ▣ " COLOR_RESET);
                                                    break;
                                                case 5:
                                                    printf(COLOR_JAUNE_BG COLOR_ROUGE"  ▣ " COLOR_RESET);
                                                    break;
                                                default:
                                                    printf(COLOR_ROUGE "  ▣ " COLOR_RESET);
                                                    break;
                                            }
                                        }else if(plateau[i][j] < 400){
                                            printf(COLOR_ROUGE_BG "    " COLOR_ROUGE "▣" COLOR_ROUGE_BG "   " COLOR_RESET );
                                        }
                                        break;
                                    default :
                                        printf("erreur1pions");
                                        break;
                                }

                            }
                        }
                    }
                    break;
                case 2:                                 //quand il y a 2 pions sur une case on regarde si c'est un barrage ou non
                    if(VerifBarrage(plateau[i][j], tab_allpions) && plateau[i][j] != -4){
                        for(int k=0; k<4;k++){                  
                            for(int l=0; l<4; l++){
                                
                                if(plateau[i][j] == tab_allpions[k][l].num_case){
                                           
                                    switch(tab_allpions[k][l].couleur){
                                        case 0:
                                            printf("🟦"); 
                                            break;
                                        case 1:
                                            printf("🟨");
                                            break;
                                        case 2:
                                            printf("🟩");
                                            break;
                                        case 3:
                                            printf("🟥");
                                            break;
                                        default :
                                            printf("erreur Barrage couleur");
                                            break;
                                    }
                                    
                                }
                            }
                        }
                    }else {                                         // si c'est pas un barrage on reduit le nombre d'espace pour que l'affichage  ne se décale pas et on met les deux pions
                        for(int k=0; k<4;k++){                  
                            for(int l=0; l<4; l++){
                                if(plateau[i][j] == tab_allpions[k][l].num_case){         
                                    switch(tab_allpions[k][l].couleur){
                                        case 0:                                            // bleu
                                            if(plateau[i][j] < 69){
                                                switch(plateau[i][j]){                     //on doit distinguer tout les cas pour les cases refuges
                                                    case 68:                            
                                                        printf(COLOR_BLANC_BG "  " COLOR_BLEU "▣" COLOR_BLANC_BG " " COLOR_RESET);
                                                        break;
                                                    case 34:
                                                        printf(COLOR_BLANC_BG "  " COLOR_BLEU "▣" COLOR_BLANC_BG " " COLOR_RESET);
                                                        break;
                                                    case 12:
                                                        printf(COLOR_BLANC_BG " " COLOR_BLEU "▣" COLOR_RESET);
                                                        break;
                                                    case 46:
                                                        printf(COLOR_BLANC_BG " " COLOR_BLEU "▣" COLOR_RESET);
                                                        break;
                                                    case 51:
                                                        printf(COLOR_BLANC_BG " " COLOR_BLEU "▣" COLOR_RESET);
                                                        break;
                                                    case 63:
                                                        printf(COLOR_BLANC_BG " " COLOR_BLEU "▣" COLOR_RESET);
                                                        break;
                                                    case 17:
                                                        printf(COLOR_BLANC_BG " " COLOR_BLEU "▣" COLOR_RESET);
                                                        break;
                                                    case 29:
                                                        printf(COLOR_BLANC_BG " " COLOR_BLEU "▣" COLOR_RESET);
                                                        break;
                                                    case -4:
                                                        printf("");
                                                        break;
                                                    case 22:
                                                        printf(COLOR_BLEU_BG " " COLOR_BLEU "▣" COLOR_RESET);
                                                        break;
                                                    case 39:
                                                        printf(COLOR_ROUGE_BG " " COLOR_BLEU "▣" COLOR_RESET);
                                                        break;
                                                    case 56:
                                                        printf(COLOR_VERT_BG " " COLOR_BLEU "▣" COLOR_RESET);
                                                        break;
                                                    case 5:
                                                        printf(COLOR_JAUNE_BG " " COLOR_BLEU "▣" COLOR_RESET);
                                                        break;
                                                        
                                                    default:
                                                        printf(COLOR_BLEU " ▣" COLOR_RESET);
                                                        break;
                                                }
                                            }else if(plateau[i][j] < 100){                              //code pour les flèches
                                            printf(COLOR_BLEU_BG " " COLOR_BLEU "▣" COLOR_RESET);
                                            }    
                                        case 1:                                                           // jaune
                                            if(plateau[i][j] < 69){
                                                switch(plateau[i][j]){
                                                    case 68:                            
                                                        printf(COLOR_BLANC_BG "  " COLOR_JAUNE "▣" COLOR_BLANC_BG " " COLOR_RESET);
                                                        break;
                                                    case 34:
                                                        printf(COLOR_BLANC_BG "  " COLOR_JAUNE "▣" COLOR_BLANC_BG " " COLOR_RESET);
                                                        break;
                                                    case 12:
                                                        printf(COLOR_BLANC_BG " " COLOR_JAUNE "▣" COLOR_RESET);
                                                        break;
                                                    case 46:
                                                        printf(COLOR_BLANC_BG " " COLOR_JAUNE "▣" COLOR_RESET);
                                                        break;
                                                    case 51:
                                                        printf(COLOR_BLANC_BG " " COLOR_JAUNE "▣" COLOR_RESET);
                                                        break;
                                                    case 63:
                                                        printf(COLOR_BLANC_BG " " COLOR_JAUNE "▣" COLOR_RESET);
                                                        break;
                                                    case 17:
                                                        printf(COLOR_BLANC_BG " " COLOR_JAUNE "▣" COLOR_RESET);
                                                        break;
                                                    case 29:
                                                        printf(COLOR_BLANC_BG " " COLOR_JAUNE "▣" COLOR_RESET);
                                                        break;
                                                    case -4:
                                                        printf("");
                                                        break;
                                                    case 22:
                                                        printf(COLOR_BLEU_BG " " COLOR_JAUNE "▣" COLOR_RESET);
                                                        break;
                                                    case 39:
                                                        printf(COLOR_ROUGE_BG " " COLOR_JAUNE "▣" COLOR_RESET);
                                                        break;
                                                    case 56:
                                                        printf(COLOR_VERT_BG " " COLOR_JAUNE "▣" COLOR_RESET);
                                                        break;
                                                    case 5:
                                                        printf(COLOR_JAUNE_BG " " COLOR_JAUNE "▣" COLOR_RESET);
                                                        break;
                                                        
                                                    default:
                                                        printf(COLOR_JAUNE " ▣" COLOR_RESET);
                                                        break;
                                                }
                                            }else if(plateau[i][j] < 200){                              //code pour les flèches
                                            printf(COLOR_JAUNE_BG "  " COLOR_JAUNE "▣" COLOR_JAUNE_BG " " COLOR_RESET);
                                            }   
                                            break;
                                        case 2:                                                      // vert
                                            if(plateau[i][j] < 69){
                                                switch(plateau[i][j]){
                                                    case 68:                            
                                                        printf(COLOR_BLANC_BG "  " COLOR_VERT "▣" COLOR_BLANC_BG " " COLOR_RESET);
                                                        break;
                                                    case 34:
                                                        printf(COLOR_BLANC_BG "  " COLOR_VERT "▣" COLOR_BLANC_BG " " COLOR_RESET);
                                                        break;
                                                    case 12:
                                                        printf(COLOR_BLANC_BG " " COLOR_VERT "▣" COLOR_RESET);
                                                        break;
                                                    case 46:
                                                        printf(COLOR_BLANC_BG " " COLOR_VERT "▣" COLOR_RESET);
                                                        break;
                                                    case 51:
                                                        printf(COLOR_BLANC_BG " " COLOR_VERT "▣" COLOR_RESET);
                                                        break;
                                                    case 63:
                                                        printf(COLOR_BLANC_BG " " COLOR_VERT "▣" COLOR_RESET);
                                                        break;
                                                    case 17:
                                                        printf(COLOR_BLANC_BG " " COLOR_VERT "▣" COLOR_RESET);
                                                        break;
                                                    case 29:
                                                        printf(COLOR_BLANC_BG " " COLOR_VERT "▣" COLOR_RESET);
                                                        break;
                                                    case -4:
                                                        printf("");
                                                        break;
                                                    case 22:
                                                        printf(COLOR_BLEU_BG " " COLOR_VERT "▣" COLOR_RESET);
                                                        break;
                                                    case 39:
                                                        printf(COLOR_ROUGE_BG " " COLOR_VERT "▣" COLOR_RESET);
                                                        break;
                                                    case 56:
                                                        printf(COLOR_VERT_BG " " COLOR_VERT "▣" COLOR_RESET);
                                                        break;
                                                    case 5:
                                                        printf(COLOR_JAUNE_BG " " COLOR_VERT "▣" COLOR_RESET);
                                                        break;
                                                    default:
                                                        printf(COLOR_VERT " ▣" COLOR_RESET);
                                                        break;
                                                }
                                            }else if(plateau[i][j] < 300){                              //code pour les flèches
                                            printf(COLOR_VERT_BG " " COLOR_VERT "▣" COLOR_RESET);
                                            }   
                                            break;
                                        case 3:                                                             // rouge
                                            if(plateau[i][j]){
                                                switch(plateau[i][j]){
                                                    case 68:                            
                                                        printf(COLOR_BLANC_BG "  " COLOR_ROUGE "▣" COLOR_BLANC_BG " " COLOR_RESET);
                                                        break;
                                                    case 34:
                                                        printf(COLOR_BLANC_BG "  " COLOR_ROUGE "▣" COLOR_BLANC_BG " " COLOR_RESET);
                                                        break;
                                                    case 12:
                                                        printf(COLOR_BLANC_BG " " COLOR_ROUGE "▣" COLOR_RESET);
                                                        break;
                                                    case 46:
                                                        printf(COLOR_BLANC_BG " " COLOR_ROUGE "▣" COLOR_RESET);
                                                        break;
                                                    case 51:
                                                        printf(COLOR_BLANC_BG " " COLOR_ROUGE "▣" COLOR_RESET);
                                                        break;
                                                    case 63:
                                                        printf(COLOR_BLANC_BG " " COLOR_ROUGE "▣" COLOR_RESET);
                                                        break;
                                                    case 17:
                                                        printf(COLOR_BLANC_BG " " COLOR_ROUGE "▣" COLOR_RESET);
                                                        break;
                                                    case 29:
                                                        printf(COLOR_BLANC_BG " " COLOR_ROUGE "▣" COLOR_RESET);
                                                        break;
                                                    case -4:
                                                        printf("");
                                                        break;
                                                    case 22:
                                                        printf(COLOR_BLEU_BG " " COLOR_ROUGE "▣" COLOR_RESET);
                                                        break;
                                                    case 39:
                                                        printf(COLOR_ROUGE_BG " " COLOR_ROUGE "▣" COLOR_RESET);
                                                        break;
                                                    case 56:
                                                        printf(COLOR_VERT_BG " " COLOR_ROUGE "▣" COLOR_RESET);
                                                        break;
                                                    case 5:
                                                        printf(COLOR_JAUNE_BG " " COLOR_ROUGE "▣" COLOR_RESET);
                                                        break;
                                                    default:
                                                        printf(COLOR_ROUGE " ▣" COLOR_RESET);
                                                        break;
                                                }
                                            }else if(plateau[i][j] < 400){                              //code pour les flèches
                                            printf(COLOR_ROUGE_BG "   " COLOR_ROUGE "▣" COLOR_ROUGE_BG " "COLOR_RESET);
                                            }   
                                            break;
                                        default :
                                            printf("erreur2pions");
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    break;
                default:                                  //plus de 2 pions on e nlève tout les espaces
                    for(int k=0; k<4;k++){                  
                        for(int l=0; l<4; l++){
                            if(plateau[i][j] == tab_allpions[k][l].num_case){         
                                switch(tab_allpions[k][l].couleur){
                                    case 0:
                                        if(plateau[i][j] < 69){
                                            switch(plateau[i][j]){                     //on doit distinguer tout les cas pour les cases refuges
                                                case 68:                            
                                                    printf(COLOR_BLANC_BG " " COLOR_BLEU "▣"  COLOR_RESET);
                                                    break;
                                                case 34:
                                                    printf(COLOR_BLANC_BG " " COLOR_BLEU "▣" COLOR_RESET);
                                                    break;
                                                case 12:
                                                    printf(COLOR_BLANC_BG  COLOR_BLEU "▣" COLOR_RESET);
                                                    break;
                                                case 46:
                                                    printf(COLOR_BLANC_BG  COLOR_BLEU "▣" COLOR_RESET);
                                                    break;
                                                case 51:
                                                    printf(COLOR_BLANC_BG  COLOR_BLEU "▣" COLOR_RESET);
                                                    break;
                                                case 63:
                                                    printf(COLOR_BLANC_BG  COLOR_BLEU "▣" COLOR_RESET);
                                                    break;
                                                case 17:
                                                    printf(COLOR_BLANC_BG  COLOR_BLEU "▣" COLOR_RESET);
                                                    break;
                                                case 29:
                                                    printf(COLOR_BLANC_BG  COLOR_BLEU "▣" COLOR_RESET);
                                                    break;
                                                case -4:
                                                    printf("");
                                                    break;
                                                case 22:
                                                    printf(COLOR_BLEU_BG COLOR_BLEU "▣" COLOR_RESET);
                                                    break;
                                                case 39:
                                                    printf(COLOR_ROUGE_BG COLOR_BLEU "▣" COLOR_RESET);
                                                    break;
                                                case 56:
                                                    printf(COLOR_VERT_BG COLOR_BLEU "▣" COLOR_RESET);
                                                    break;
                                                case 5:
                                                    printf(COLOR_JAUNE_BG COLOR_BLEU "▣" COLOR_RESET);
                                                    break;
                                                default:
                                                    printf(COLOR_BLEU "▣" COLOR_RESET);
                                                    break;
                                            }
                                        }else if(plateau[i][j] < 100){                              //code pour les flèches
                                        printf(COLOR_BLEU_BG COLOR_BLEU "▣" COLOR_RESET);
                                        }    
                                    case 1:
                                        if(plateau[i][j] < 69){
                                            switch(plateau[i][j]){
                                                case 68:                            
                                                    printf(COLOR_BLANC_BG " " COLOR_JAUNE "▣" COLOR_RESET);
                                                    break;
                                                case 34:
                                                    printf(COLOR_BLANC_BG " " COLOR_JAUNE "▣" COLOR_RESET);
                                                    break;
                                                case 12:
                                                    printf(COLOR_BLANC_BG COLOR_JAUNE "▣" COLOR_RESET);
                                                    break;
                                                case 46:
                                                    printf(COLOR_BLANC_BG  COLOR_JAUNE "▣" COLOR_RESET);
                                                    break;
                                                case 51:
                                                    printf(COLOR_BLANC_BG COLOR_JAUNE "▣" COLOR_RESET);
                                                    break;
                                                case 63:
                                                    printf(COLOR_BLANC_BG COLOR_JAUNE "▣" COLOR_RESET);
                                                    break;
                                                case 17:
                                                    printf(COLOR_BLANC_BG COLOR_JAUNE "▣" COLOR_RESET);
                                                    break;
                                                case 29:
                                                    printf(COLOR_BLANC_BG COLOR_JAUNE "▣" COLOR_RESET);
                                                    break;
                                                case -4:
                                                    printf("");
                                                    break;
                                                case 22:
                                                    printf(COLOR_BLEU_BG COLOR_JAUNE "▣" COLOR_RESET);
                                                    break;
                                                case 39:
                                                    printf(COLOR_ROUGE_BG COLOR_JAUNE "▣" COLOR_RESET);
                                                    break;
                                                case 56:
                                                     printf(COLOR_VERT_BG COLOR_JAUNE "▣" COLOR_RESET);
                                                    break;
                                                case 5:
                                                    printf(COLOR_JAUNE_BG COLOR_JAUNE "▣" COLOR_RESET);
                                                    break;
                                                default:
                                                    printf(COLOR_JAUNE "▣" COLOR_RESET);
                                                    break;
                                            }
                                        }else if(plateau[i][j] < 200){                              //code pour les flèches
                                        printf(COLOR_JAUNE_BG " " COLOR_JAUNE "▣" COLOR_RESET);
                                        }   
                                        break;
                                    case 2:
                                        if(plateau[i][j] < 69){
                                            switch(plateau[i][j]){
                                                case 68:                            
                                                    printf(COLOR_BLANC_BG " " COLOR_VERT "▣" COLOR_RESET);
                                                    break;
                                                case 34:
                                                    printf(COLOR_BLANC_BG " " COLOR_VERT "▣" COLOR_RESET);
                                                    break;
                                                case 12:
                                                    printf(COLOR_BLANC_BG COLOR_VERT "▣" COLOR_RESET);
                                                    break;
                                                case 46:
                                                    printf(COLOR_BLANC_BG COLOR_VERT "▣" COLOR_RESET);
                                                    break;
                                                case 51:
                                                    printf(COLOR_BLANC_BG COLOR_VERT "▣" COLOR_RESET);
                                                    break;
                                                case 63:
                                                    printf(COLOR_BLANC_BG COLOR_VERT "▣" COLOR_RESET);
                                                    break;
                                                case 17:
                                                    printf(COLOR_BLANC_BG COLOR_VERT "▣" COLOR_RESET);
                                                    break;
                                                case 29:
                                                    printf(COLOR_BLANC_BG COLOR_VERT "▣" COLOR_RESET);
                                                    break;
                                                case -4:
                                                    printf("");
                                                    break;
                                                case 22:
                                                    printf(COLOR_BLEU_BG COLOR_VERT "▣" COLOR_RESET);
                                                    break;
                                                case 39:
                                                    printf(COLOR_ROUGE_BG COLOR_VERT "▣" COLOR_RESET);
                                                    break;
                                                case 56:
                                                    printf(COLOR_VERT_BG COLOR_VERT "▣" COLOR_RESET);
                                                    break;
                                                case 5:
                                                    printf(COLOR_JAUNE_BG COLOR_VERT "▣" COLOR_RESET);
                                                    break;
                                                default:
                                                    printf(COLOR_VERT "▣" COLOR_RESET);
                                                    break;
                                            }
                                        }else if(plateau[i][j] < 300){                              //code pour les flèches
                                        printf(COLOR_VERT_BG COLOR_VERT "▣" COLOR_RESET);
                                        }   
                                        break;
                                    case 3:
                                        if(plateau[i][j]){
                                            switch(plateau[i][j]){
                                                case 68:                            
                                                    printf(COLOR_BLANC_BG " " COLOR_ROUGE "▣" COLOR_RESET);
                                                    break;
                                                case 34:
                                                    printf(COLOR_BLANC_BG " " COLOR_ROUGE "▣"  COLOR_RESET);
                                                    break;
                                                case 12:
                                                    printf(COLOR_BLANC_BG COLOR_ROUGE "▣" COLOR_RESET);
                                                    break;
                                                case 46:
                                                    printf(COLOR_BLANC_BG COLOR_ROUGE "▣" COLOR_RESET);
                                                    break;
                                                case 51:
                                                    printf(COLOR_BLANC_BG COLOR_ROUGE "▣" COLOR_RESET);
                                                    break;
                                                case 63:
                                                    printf(COLOR_BLANC_BG COLOR_ROUGE "▣" COLOR_RESET);
                                                    break;
                                                case 17:
                                                    printf(COLOR_BLANC_BG COLOR_ROUGE "▣" COLOR_RESET);
                                                    break;
                                                case 29:
                                                    printf(COLOR_BLANC_BG COLOR_ROUGE "▣" COLOR_RESET);
                                                    break;
                                                case -4:
                                                    printf("");
                                                    break;
                                                case 22:
                                                    printf(COLOR_BLEU_BG COLOR_ROUGE "▣" COLOR_RESET);
                                                    break;
                                                case 39:
                                                    printf(COLOR_ROUGE_BG COLOR_ROUGE "▣" COLOR_RESET);
                                                    break;
                                                case 56:
                                                    printf(COLOR_VERT_BG COLOR_ROUGE "▣" COLOR_RESET);
                                                    break;
                                                case 5:
                                                    printf(COLOR_JAUNE_BG COLOR_ROUGE "▣" COLOR_RESET);
                                                    break;
                                                default:
                                                    printf(COLOR_ROUGE "▣" COLOR_RESET);
                                                    break;
                                            }
                                        }else if(plateau[i][j] < 400){                              //code pour les flèches
                                        printf(COLOR_ROUGE_BG " " COLOR_ROUGE "▣" COLOR_RESET);
                                        }   
                                        break;
                                    default :
                                        printf("erreurnbr default de pion");
                                        break;
                                }
                            }
                        }
                    }
                break;
            }
        }
            
        printf("\n");                               //Cette partie du code rèpete les étapes précedent sans afficher les chiffre ou pions pour faire une ligen vide entre chaque case pour fare plus d'espace
        for(int j=0; j<18; j++){
            if(plateau[i][j] < 69){
                    switch(plateau[i][j]){
                            case 22:
                                printf(COLOR_BLEU_BG "    " COLOR_RESET);
                                break;
                            case 39:
                                printf(COLOR_ROUGE_BG "    " COLOR_RESET);
                                break;
                            case 56:
                                printf(COLOR_VERT_BG "    " COLOR_RESET);
                                break;
                            case 5:
                                printf(COLOR_JAUNE_BG "    " COLOR_RESET);
                                break;
                            case -5:
                                printf(COLOR_BLEU_BG "    " COLOR_RESET);
                                break;
                            case -6:
                                printf(COLOR_JAUNE_BG "    " COLOR_RESET);
                                break;
                            case -7:
                                printf(COLOR_VERT_BG "    " COLOR_RESET);
                                break;
                            case -8:
                                printf(COLOR_ROUGE_BG "    " COLOR_RESET);
                                break;
                            case 68:
                                printf(COLOR_BLANC_BG "        " COLOR_RESET);
                                break;
                            case 34:
                                printf(COLOR_BLANC_BG "        " COLOR_RESET);
                                break;
                            case 12:
                                printf(COLOR_BLANC_BG "    " COLOR_RESET);
                                break;
                            case 46:
                                printf(COLOR_BLANC_BG "    " COLOR_RESET);
                                break;
                            case 51:
                                printf(COLOR_BLANC_BG "    " COLOR_RESET);
                                break;
                            case 63:
                                printf(COLOR_BLANC_BG "    " COLOR_RESET);
                                break;
                            case 17:
                                printf(COLOR_BLANC_BG "    " COLOR_RESET);
                                break;
                            case 29:
                                printf(COLOR_BLANC_BG "    " COLOR_RESET);
                                break;
                            default:
                                printf("    ");
                }
            }else if(plateau[i][j] < 100){                          //code pour les fleches finale
                printf(COLOR_BLEU_BG "    " COLOR_RESET);

            }else if(plateau[i][j] < 200){
                printf(COLOR_JAUNE_BG "        " COLOR_RESET );

            }else if(plateau[i][j] < 300){
                printf(COLOR_VERT_BG "    " COLOR_RESET);

            }else if(plateau[i][j] < 400){
                printf(COLOR_ROUGE_BG "        " COLOR_RESET );

            }
            
        }
            
        printf("\n");
    }

    affPanier(tab_allpions);
}
/* Auteur : Dorian*/
/* Entrée(s) : le joueur en question */      
void afficherPionsJoueur(Joueur j)
{
    for (int i=0; i<4; i++)
    {
        switch (j.tab_pions[i].num_case)
        {
            case 169:
            printf(COLOR_JAUNE "Position du pion %d : 1 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 269:
            printf(COLOR_VERT "Position du pion %d : 1 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 369:
            printf(COLOR_ROUGE "Position du pion %d : 1 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 170:
            printf(COLOR_JAUNE "Position du pion %d : 2 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 270:
            printf(COLOR_VERT "Position du pion %d : 2 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 370:
            printf(COLOR_ROUGE "Position du pion %d : 2 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 171:
            printf(COLOR_JAUNE "Position du pion %d : 3 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 271:
            printf(COLOR_VERT "Position du pion %d : 3 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 371:
            printf(COLOR_ROUGE "Position du pion %d : 3 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 172:
            printf(COLOR_JAUNE "Position du pion %d : 4 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 272:
            printf(COLOR_VERT "Position du pion %d : 4 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 372:
            printf(COLOR_ROUGE "Position du pion %d : 4 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 173:
            printf(COLOR_JAUNE "Position du pion %d : 5 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 273:
            printf(COLOR_VERT "Position du pion %d : 5 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 373:
            printf(COLOR_ROUGE "Position du pion %d : 5 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 174:
            printf(COLOR_VERT "Position du pion %d : 6 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 274:
            printf(COLOR_VERT "Position du pion %d : 6 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 374:
            printf(COLOR_ROUGE "Position du pion %d : 6 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 175:
            printf(COLOR_JAUNE "Position du pion %d : 7 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 275:
            printf(COLOR_VERT "Position du pion %d : 7 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 375:
            printf(COLOR_ROUGE "Position du pion %d : 7 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 176:
            printf(COLOR_JAUNE "Position du pion %d : 8 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 276:
            printf(COLOR_VERT "Position du pion %d : 8 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 376:
            printf(COLOR_ROUGE "Position du pion %d : 8 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 69:
            printf(COLOR_BLEU "Position du pion %d : 1 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 70:
            printf(COLOR_BLEU "Position du pion %d : 2 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 71:
            printf(COLOR_BLEU "Position du pion %d : 3 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 72:
            printf(COLOR_BLEU "Position du pion %d : 4 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 73:
            printf(COLOR_BLEU "Position du pion %d : 5 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 74:
            printf(COLOR_BLEU "Position du pion %d : 6 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 75:
            printf(COLOR_BLEU "Position du pion %d : 7 sur la flèche\n" COLOR_RESET, i+1);
            break;
            case 76:
            printf(COLOR_BLEU "Position du pion %d : 8 sur la flèche\n" COLOR_RESET, i+1);
            break;
            default:
            switch (j.couleur)
            {
                case 0:
                printf(COLOR_BLEU "Position du pion %d : %d\n" COLOR_RESET, i+1, j.tab_pions[i].num_case);
                break;
                case 1:
                printf(COLOR_JAUNE "Position du pion %d : %d\n" COLOR_RESET, i+1, j.tab_pions[i].num_case);
                break;
                case 2:
                printf(COLOR_VERT "Position du pion %d : %d\n" COLOR_RESET, i+1, j.tab_pions[i].num_case);
                break;
                case 3:
                printf(COLOR_ROUGE "Position du pion %d : %d\n" COLOR_RESET, i+1, j.tab_pions[i].num_case);
                break;
            }
            break;
        }
    }
}
/* Auteur : Dorian*/
/* Entrée(s) : le tableau de tout les pions */
void affPanier(Pion** tab_allpions){
    printf("Voici tous les pions arrivés  \n");
    printf("――――――――――― \n");
    for(int i=0; i<4;i++){
        printf("| ");
        for(int j=0; j<4; j++){
            if(tab_allpions[i][j].num_case == -4){
                switch(tab_allpions[i][j].couleur){
                    case 0:
                        printf(COLOR_BLEU "▣ " COLOR_RESET);
                        break;
                    case 1:
                        printf(COLOR_JAUNE "▣ " COLOR_RESET);
                        break;
                    case 2:
                        printf(COLOR_VERT "▣ " COLOR_RESET);
                        break;
                    case 3:
                        printf(COLOR_ROUGE "▣ " COLOR_RESET);
                        break;
                    default:
                        printf("erreur panier code couleurs");
                        break;
                }
            }else {
                printf(COLOR_BLANC_BG " " COLOR_RESET " ");
            }
        }
        printf("| \n");
        printf("――――――――――― \n");
    }
}
