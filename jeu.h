
/* Auteur : Dorian */
/* Date :  23/05/2024 */
/* Résumé : Structures initialisées */

#ifndef __jeu_H_
#define __jeu_H_


typedef struct
{
    int couleur; // 0 = bleu, 1 = jaune, 2 = vert, 3 = rouge
    int nb_case_franchies;
    int num_case;
} Pion;

typedef struct
{
    Pion* tab_pions; // liste des pions du joueur
    int couleur; // 0 = bleu, 1 = jaune, 2 = vert, 3 = rouge
    int humain; // dit si le joueur est humain (0 : non, 1 : oui)
    int compteur_six;
    int compteur_six_daffilee;
    int nb_pions_sortis;
    int nb_pions_arrives;
    Pion dernier_pion_joue;
    int num_dernier_pion_joue;
    int aBarrage;
} Joueur;




void clear(void);
char* HumainOuOrdi(int nbJoueurs);
Pion initPion(int couleur);
Joueur initJoueur(char reponse, int couleur);
Joueur* InitTabJoueurs(char* tabreponse);
int lancerD(void);
int choixPion(void);
void GererReglesSix(Joueur* tab_joueurs, Joueur j, Pion** tab_allpions, int num_j, int* a_gagne, int* gagnant);
void GererCasesFin(Pion* tab_pions, int num_pion);
void Jouer(Joueur* tab_j, Joueur j, int valeur_de, Pion** tab_allpions, int num_j, int* a_gagne, int* gagnant);
void JouerRobot(Joueur* tab_j, Joueur j, int valeur_de, Pion** tab_allpions, int num_j, int* a_gagne, int* gagnant);
int PeutJouer(Pion** tab_allpions, Pion pion, int valeur_de);
int CompterPionsCase(int caze, Pion** tab_allpions);
Pion* PionsSurCaze(int caze, Pion** tab_allpions);
#endif