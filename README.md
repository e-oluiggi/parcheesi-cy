# Exécution du programme : 
    make && ./exe

Durant votre partie, il se peut que lorsque vous ferez 6 et qu'il y aura un pion sur votre case de départ, on vous redemande le pion que vous voulez jouer, il faudra alors le ressaisir.
Aussi si aucun de vos pions ne peut se déplacer, il faudra saisir 5 chiffres valides pour que votre tour soit passé.

# Les règles du jeu : 

- Sur les 68 cases, douze de ces cases sont des cases refuges (indiquées par une case de couleur sur l'image illustrant cet article) où les pions ne peuvent pas être mangés par un pion adverse.
- Le joueur en bleu commence la partie.
- On tourne dans le sens des aiguilles d'une montre.
- On avance ses pions en lançant le dé chacun son tour. Un tour commence dès qu'un joueur a lancé le dé. 
- Pour sortir de la maison, il faut faire un six ; la première fois que l'on fait 6, on doit sortir un pion. On est dans l'obligation de sortir un pion si un pion adverse occupe la maison lorsque l'on fait 6. Dans ce cas, le pion adverse est mangé et donne un bonus de 20 cases pour l'avoir mangé.
- Deux pions d'un même joueur qui sont sur la même case forment un barrage infranchissable (sauf si ce barrage est placé sur une case de sortie et que celui-ci tire un 6).
- Quand un joueur fait 6, il doit obligatoirement ouvrir son barrage. Il avance de 6 cases quand il n'a pas sorti tous ses pions de sa maison et de 7 cases quand il a sorti tous ses pions. On peut faire un maximum de deux 6 d’affilée. Si l'on fait un troisième 6, le pion précédemment joué rentre à la maison sauf si le pion joué est sur une case refuge.
- Pour rentrer son pion, il faut impérativement faire le chiffre exact. Si ce nombre est dépassé, on ne peut pas jouer avec ce pion.
- Si aucun pion ne peut être déplacé, le joueur passe son tour.
- Quand on utilise son pion en bonus, on doit avancer un autre pion de 10 cases.
- Manger n'est pas obligatoire, chaque joueur n'est pas dans l'obligation de manger, que ce soit d'un simple coup de dé, en mangeant en 20, ou encore en en mangeant en 10 avec son bonus. Quand on mange un pion adverse, on doit avancer de 20 cases un pion au choix.


## Ce jeu de Parcheesi a été réalisé par : 
- AOUN Moatez
- CAZENAVE Dorian
- GUILLERAND Mahtis
- LUIGGI Oscar